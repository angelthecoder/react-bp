## Construido con :file_folder:

_Estructura de Folders para el proyecto_

- Assets - Elementos extra como JS, CSS, fonts e imágenes
- Components - Aquí pasa la mayoría del trabajo, hasta el fondo tenemos nuestro componenente principal Root que mantendra nuestra app junta
  - Feature - Todo lo que necesitamos para hacer una página en nuestra app
  - Layout - Estos folders incluyen todos los componentes que maquillan nuestra app
  - Shared - Todos los elementos que se van a repetir dentro de nuestra aplicación
- Hooks, Reducer, Router, Themes - VIP mantendrá nuestro código limpi e incluyen elementos de setup
- jsconfig.json & package.json - Archivos esenciales de configuración que casi nunca tocamos
